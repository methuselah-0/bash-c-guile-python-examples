#!/usr/bin/env guile
!#
(use-modules
 (ice-9 popen) ;; open-input-pipe
 (ice-9 rdelim) ;; read-line
 (gnu bash)
 )
;; Load c code
(define readf-lib (dynamic-link "./libcio.so"))
(dynamic-call "init_cio" readf-lib)
;; Note the '/'-prefix and blankspace postfix
(define bashmod "/python-on-guile-test.sh-help.scm ")

(define-bash-function (f1_py x)
  (let* ((lines (open-input-pipe (string-append
                                 "guile -e f1_py_bash "
                                 (dirname (current-filename))
                                 bashmod x)))
        (result (read-string lines))
        )
    (write-file "./result.txt" result)
    (display result)))
;; Interactive guile stuff, but 'f' is still python.
(define-bash-function (f2_py)
  (system (string-append "guile -e f2_py_bash "
                         (dirname (current-filename))
                         bashmod)))

;; This does not work!
;; (define-bash-function (f_py2_bash_pybin x)
;;    (display (number->string (f 2))))
