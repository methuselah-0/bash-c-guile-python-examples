#include <libguile.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* //char *readFile(char *filename) { */
/* static SCM */
/* readFile(SCM file) { */
/*   char * filename = scm_to_utf8_string(file); */
/*   FILE *f = fopen(filename, "rt"); */
/*   assert(f); */
/*   fseek(f, 0, SEEK_END); */
/*   long length = ftell(f); */
/*   fseek(f, 0, SEEK_SET); */
/*   char *buffer = (char *) malloc(length + 1); */
/*   buffer[length] = '\0'; */
/*   fread(buffer, 1, length, f); */
/*   fclose(f); */
/*   // return buffer; */
/*   return scm_from_utf8_string(buffer); */
/* } */

void
writeFile(char *filename, char *string) {
   FILE *fp;
   fp = fopen(filename, "w+");
   //fprintf(fp, string);
   fputs(string, fp);
   fclose(fp);
}

static SCM
writeFile_wrap (SCM file, SCM scmstring)
{
  char *filename = scm_to_utf8_string(file);
  char *string = scm_to_utf8_string(scmstring);
  writeFile (filename, string);
  return SCM_UNSPECIFIED;
}

char *readFile(char *filename) {
  FILE *f = fopen(filename, "rt");
  assert(f);
  fseek(f, 0, SEEK_END);
  long length = ftell(f);
  fseek(f, 0, SEEK_SET);
  char *buffer = (char *) malloc(length + 1);
  buffer[length] = '\0';
  fread(buffer, 1, length, f);
  fclose(f);
  return buffer;
}

static SCM
readFile_wrap (SCM file)
{
  char *filename = scm_to_utf8_string(file);
  return scm_from_utf8_string( readFile (filename));
}

/* static void* */
/* register_functions (void* data) */
/* { */
/*   scm_c_define_gsubr("read-file", 1, 0, 0, &readFile); */
/*   return NULL; */
/* } */

/* int main (int argc, char* argv[]){ */
/* // int main() { */
/*   scm_with_guile (&register_functions, NULL); */
/*   //scm_shell (argc, argv); */
/*   //char *content = readFile("./hello.txt"); */
/*   //printf("%s", content); */
/* } */

void
init_cio (void *unused)
{
  scm_c_define_gsubr("read-file", 1, 0, 0, &readFile_wrap);
  scm_c_export ("read-file", NULL);
  scm_c_define_gsubr("write-file", 2, 0, 0, &writeFile_wrap);
  scm_c_export ("write-file", NULL);
}

void
scm_init_cio_module ()
{
  scm_c_define_module ("cio", init_cio, NULL);
}
