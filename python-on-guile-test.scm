#!/run/current-system/profile/bin/guile
!#

;; to compile python-on-guile-test, run: guild compile python-on-guile-test.py --output=python-on-guile-test.go --from=python
(add-to-load-path (dirname (current-filename)))

(define-module (python-on-guile-test))
(load-compiled "python-on-guile-test.go")

(define-public (f1_py a)
  (f a)
)
(define-public (f2_py)
  (display "Enter a number: ")
  (let (
        (a (read))
        )
    (display (string-append  "f of a is: " (number->string (f a)) "\n"))))
