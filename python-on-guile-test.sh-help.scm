#!/usr/bin/env guile
!#
(add-to-load-path (dirname (current-filename)))
(use-modules (python-on-guile-test))

(define-public (f1_py_bash cmdline)
  (let (
        (arg1 (cadr (command-line)))
        )
    (display (f1_py (string->number (cadr cmdline))))))

(define-public (f2_py_bash cmdline)
  (f2_py))
