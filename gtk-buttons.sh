#!/bin/bash

# This is a port of the GTK+3 Hello World to bash.
#
# https://developer.gnome.org/gtk3/stable/gtk-getting-started.html
source ctypes.sh

# declare some numeric constants used by GTK+
declare -ri GTK_ORIENTATION_HORIZONTAL=0
declare -ri G_APPLICATION_FLAGS_NONE=0
declare -ri G_CONNECT_AFTER=$((1 << 0))
declare -ri G_CONNECT_SWAPPED=$((1 << 1))
declare -ri SHOW_CLOSE_BUTTON_TRUE=1
declare -ri LABEL_LINE_WRAP_TRUE=1

declare -ri _TRUE=0
declare -ri _FALSE=1
# void print_hello(GtkApplication *app, gpointer user_data)

# Add guile and python code
enable -f ~/.guix-profile/lib/bash/libguile-bash.so scm
scm $(pwd)/python-on-guile-test.sh.scm

function print_hello ()
{
    ttype=$(dlcall printf '%u' $ttype2)
    #ttype=$(dlcall puts $ttype2)
    #echo "Hello $hdybar type | $ttype | $ttype2 | World"
    #echo "Hello World"
    printf '%s' "5 times 2 is "
    f1_py 5
    echo
}

# void activate(GtkApplication *app, gpointer user_data)
function activate ()
{
    local app=$2
    local user_data=$3
    local window
    local button
    local button_box

    dlsym -n gtk_widget_destroy gtk_widget_destroy

    # Create window
    dlcall -n window -r pointer gtk_application_window_new $app
    dlcall gtk_window_set_title $window "MyWindowTitle"
    dlcall gtk_window_set_default_size $window 200 200
    # How to unpack this var?
    #dlcall -n ttype -r pointer gtk_window_get_title $window

    # Create title and header bars
    dlcall -n title_bar -r pointer hdy_title_bar_new $app
    dlcall -n header_bar -r pointer gtk_header_bar_new $app
    # Specify the header bar
    dlcall gtk_header_bar_set_title $header_bar "MyHeader_BarTitle"
    dlcall gtk_header_bar_set_show_close_button $header_bar $SHOW_CLOSE_BUTTON_TRUE

    # Add the header bar to the title bar and the title_bar to the window
    dlcall gtk_container_add $title_bar $header_bar
    dlcall gtk_window_set_titlebar $window $title_bar

    # Create a label
    dlcall -n label -r pointer gtk_label_new $window
    #dlcall gtk_label_set_label $window
    dlcall gtk_label_set_line_wrap $label $LABEL_LINE_WRAP_TRUE

    # Specify the label text
    dlcall gtk_label_set_text $label "This example shows how to use a libhandy title bar to hold a regular header bar."
    #dlcall gtk_label_set_markup $label "<big>This example shows how to use a libhandy title bar to hold a regular header bar.</big>"

    # Create a grid
    dlcall -n grid -r pointer gtk_grid_new $window

    # Add the label
    #dlcall gtk_container_add $window $label
    #dlcall gtk_container_add $grid $label
    dlcall gtk_grid_attach $grid $label 0 0 2 1

    # Create 2 button boxes and add them to the window
    #dlcall -n button_box1 -r pointer gtk_button_box_new $GTK_ORIENTATION_HORIZONTAL
    #dlcall -n button_box2 -r pointer gtk_button_box_new $GTK_ORIENTATION_HORIZONTAL

    #dlcall gtk_container_add $window $button_box1
    #dlcall gtk_container_add $grid $button_box1
    #dlcall gtk_grid_attach $grid $button_box1 0 0 1 1
    ##dlcall gtk_container_add $window $button_box2

    # Create a "do stuff button"
    dlcall -n dobutton -r pointer gtk_button_new_with_label "Do Stuff"
    dlcall g_signal_connect_data $dobutton "clicked" $bash_print_hello $NULL $NULL 0
    dlcall gtk_grid_attach $grid $dobutton 0 1 1 1
    
    # Create an exit button
    dlcall -n exitbutton -r pointer gtk_button_new_with_label "Exit"
    dlcall g_signal_connect_data $exitbutton "clicked" $gtk_widget_destroy $window $NULL $G_CONNECT_SWAPPED
    dlcall gtk_grid_attach $grid $exitbutton 1 1 1 1
    
    # Add buttons
    #dlcall gtk_container_add $button_box1 $dobutton
    #dlcall gtk_container_add $button_box1 $exitbutton
    #dlcall gtk_container_add $button_box2 $exitbutton

    dlcall gtk_container_add $window $grid
    # Show all containers recursively
    dlcall gtk_widget_show_all $window
}

declare app     # GtkApplication *app
declare status  # int status

# Generate function pointers that can be called from native code.
callback -n bash_print_hello print_hello void pointer pointer
callback -n bash_activate activate void pointer pointer

# Prevent threading issues.
taskset -p 1 $$ &> /dev/null

# Make libgtk-3 symbols available
#dlopen libgtk-3.so.0
dlopen /gnu/store/snvn45h52q74ssds70m35g7sya8pmzf7-gtk+-3.24.10/lib/libgtk-3.so
#dlopen ~/VirtualHome/src/Projects/libhandy/_build/src/libhandy-0.0.so
dlopen /gnu/store/yjka5047zxi4lnq0hkrc5a7k0d7jbmga-libhandy-0.0.12/lib/libhandy-0.0.so

dlcall -n app -r pointer gtk_application_new "org.gtk.example" $G_APPLICATION_FLAGS_NONE
dlcall -r ulong g_signal_connect_data $app "activate" $bash_activate $NULL $NULL 0
dlcall -n status -r int g_application_run $app 0 $NULL
dlcall g_object_unref $app

exit ${status##*:}
